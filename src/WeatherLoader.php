<?php

namespace App;

use SimpleXMLElement;

class WeatherLoader
{
    public function getWeatherForLocation($location)
    {
        $xml = $this->fetchData($location);
        file_put_contents(__DIR__ . '/../weather.log', 'Fetched XML: ' . $xml . "\n======", FILE_APPEND);
        return $this->parseData($xml);
    }

    private function fetchData($city)
    {
        $url = sprintf('http://weather.suora.training/ig/api?weather=%s', $city);
        return file_get_contents($url);
    }

    protected function parseData($xml)
    {
        $xml3 = new SimpleXMLElement($xml);

        $weather = new Weather();
        $weather->temperatur = (string) $xml3->weather->current_conditions->temp_c['data'];
        $weather->condition =  (string) $xml3->weather->current_conditions->condition['data'];
        $weather->windCondition = (string) $xml3->weather->current_conditions->wind_condition['data'];
        return $weather;
    }
}