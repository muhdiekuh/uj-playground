<?php

use App\WeatherLoader;

// require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../src/Weather.php';
require_once __DIR__ . '/../src/WeatherLoader.php';

$loader = new WeatherLoader();
$weather = $loader->getWeatherForLocation("Bamberg");

echo "In Bamberg ist es " . $weather->temperatur . " Grad und " . $weather->condition . ".";
